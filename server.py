#!/usr/bin/python
# -*- coding: utf-8 -*-

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
import SocketServer, webbrowser
import sys,os,re,time,copy
import StringIO,traceback
from urlparse import *
import socket
from threading import Timer

reload(sys)
sys.setdefaultencoding("utf-8")

echo=None

class ThreadingHTTPServer(ThreadingMixIn, HTTPServer):
    pass

class S(BaseHTTPRequestHandler):
    def __init__(self,*args):
        self.HOME_HTML = "files"
        self.CORE      = "core"
        #configuration @ CORE
        self.SETTINGS  = "settings"
        #bootstrap     @ HOME_HTML
        self.BOOTSTRAP = "bootstrap"
        self.HEADER    = "header"
        self.FOOTER    = "footer"
        #synamic scripts used by the core
        self.INDEX_OF  = "indexof"
        self.OUT       = "out"
        #and some 'needed' variables
        self.mypath    = self.HOME_HTML
        self.nl        = "\n"
        self.lines     = [] # for console output
        self.o         = None  #url parsed
        self.q         = None  #query values (GET)
        self.GET       = {"mode":"html"}  #dict with GET var,vals
        self.curdir    = None
        self.output    = [] # items/html that will be sent to the client
        self.send_now  = False # used for redirects
        self.flushed   = False # to control flush+headers ops.        
        self.load_settings()
        BaseHTTPRequestHandler.__init__(self,*args)
    def out(self,data,**kargs):
        self.args={}
        self.args["data"]  = data
        self.args["kargs"] = kargs
        res=self.include("../"+self.CORE+"/"+self.OUT,debug=False)
        if len(res): print res
    def flush(self):
        self.flushed=True
        for item in self.output:
            try:
                self.wfile.write(item)
            except Exception,e:
                pass
        self.output=[]
    def escape(self,html):
        """Returns the given HTML with ampersands, quotes and carets encoded."""
        return str(html).replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;').replace("'", '&#39;')
    def echo(self,data):
        self.console(data)
    def console(self,msg,style="console",caller="unknown_console_caller",**kargs):
        style =kargs.get("style" ,style)
        caller=kargs.get("caller",caller)
        self.lines.append(msg)
        return self.beauty(msg,style=style,caller=caller)
    def beauty(self,msg,caller="unknown_caller",style="default",**kargs):
        caller=kargs.get("caller",caller)
        style =kargs.get("style" ,style)
        msg="<b><span class='%s'>%s</span></b>"%(style,msg)
        template=self.BOOTSTRAP+"/styles/%s"%style
        if os.path.exists(template):
            msg=self.include(template,console=False)
        else:
            msg+="<div class=info>template '%s' not found for decorate this message ^_^ (%s style=%s ?)</div>"%(template,caller,style)
        return msg
    def _set_headers(self):
        if not self.flushed:
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
    def redir(self,url):
        self.send_response(302)
        self.send_header('Location', url)
        self.end_headers()
        self.send_now=True
    def read(self,url,mode="html",mark=None,cookies={},timeout=5):
        import requests,json
        try:
            r = requests.get(url, cookies=cookies,timeout=timeout)
            data=r.text
            if mode=="json":
                data=r.text[r.text.index(mark)+len(mark)+1:-2]
                data=json.loads(data)
            return data
        except Exception,e:
            return "[read error] %s"%str(e)
    def load_settings(self,debug=False):
        self.include("../"+self.CORE+"/"+self.SETTINGS,debug=debug)
    def do_GET(self):
        self.flushed = False
        self.output  = []
        filename = self.path[1:]
        self.o   = urlparse(filename)
        self.q   = parse_qsl(self.o.query)
        for k,v in self.q:
            self.GET[k]=v
        self.load_settings(debug=False)     
        if self.GET["mode"]!="raw":
            aout_header  = self.include( self.BOOTSTRAP+"/"+self.HEADER , show_notfound=False) # header is python code
            aout_results = self.include( filename , to_stdout=True)
            if self.send_now:
                self.out(aout_results,clean=True)
                self.flush()
                self.send_now=False
                return
            aout_footer  = self.include( self.BOOTSTRAP+"/"+self.FOOTER , show_notfound=False)
            aout=   aout_header+\
                    aout_results+\
                    aout_footer
        else:
            aout =self.include( filename , to_stdout=True)
        self._set_headers()
        self.out(aout)
        self.flush()
    def do_HEAD(self):
        self._set_headers()
    def do_POST(self):
        self._set_headers()
        self.wfile.write("<html><body><h1>#TODO!</h1></body></html>")
    def include(self,filename,execute=True,debug=False,to_stdout=False,
        console=False,show_notfound=True):
        code=""
        if show_notfound:
            code="[include('%s') guess code was not found]"%filename
            code="print \"%s\""%code
        self.o = urlparse(filename)
        self.q = parse_qsl(self.o.query)
        for k,v in self.q:
            self.GET[k]=v
        if debug:
            print "include_debug: filename: %s execute: %s"%(filename,execute)
        # create file-like string to capture output
        codeOut = StringIO.StringIO()
        filename=self.o.path
        if not len(filename):
            filename=self.CORE+"/"+self.INDEX_OF
            fullpath=filename
        else:
            fullpath="%s%s%s"%(
                self.HOME_HTML,
                "/" if not filename.startswith("/") else "",
                filename)
        self.curdir=self.HOME_HTML
        if debug:
            print "include_debug: curdir %s filename %s"%(self.curdir,filename)
        if os.path.isdir(fullpath):
            self.curdir = fullpath 
            fullpath    = "%s/%s"%(self.CORE,self.INDEX_OF)
            print 'directory %s reached'%fullpath
            if filename[-1]!="/":
                self.redir("/"+filename+"/")
        if os.path.exists(fullpath):
            if os.path.isdir(fullpath):
                code="print 'contents hidden because %s is a dir'"%fullpath
                
            else:
                code=open(fullpath,"rb").read()
        else:
            msg="include_error: \"%s\" not found..."%fullpath
            if console:
                code=self.console(msg,caller="include_error")
        fullpath=re.sub(self.HOME_HTML+"/../","",fullpath)
        # capture output and errors
        sys.stdout = codeOut

        try:
            if execute:
                self.echo("exe: %s"%fullpath)
                exec code
            else:
                #self.echo(code)
                print code
        except Exception,e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print "<pre>"
            print "include_exception: can't run \"%s\""%fullpath
            print "*** print_tb:"
            traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            print "*** print_exception:"
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print "_BEGIN_OF_CODE_"
            print "<div class=boc>"
            try: 
                for i,line in enumerate(self.escape(code).split("\n")):
                    print "%d: %s"%(i+1,line)
            except Exception,e:
                print self.escape(str(e))
            print "</div>   "
            print "__END_OF_CODE__"
            print "execute=%s"%execute
            if exc_type == TypeError:
                print "<div class=hexdump>"
                print self.hexdump(code)
                print "</div>"
            
        msg_out = codeOut.getvalue()
        # restore stdout
        sys.stdout = sys.__stdout__

        codeOut.close()
        if not execute:
            msg_out=code
        if to_stdout:
            self.console(msg_out)
            print self.lines
            self.lines=[]
        return msg_out
    def callApp(self,appname,params={},mode="raw"):
        html="%s params: %s"%(appname,params)
        for k,v in params.iteritems():
            self.GET[k]=v
        saved=copy.copy(self.output)
        include_full=""
        if not appname.startswith("/"):
            if not self.o.path.startswith("/"):
                include_full+="/"
            include_full+=self.o.path
            if not include_full.endswith("/"):
                include_full+="/"
        include_full+="%s?mode=%s"%(appname,mode)
        if len(self.o.query):
            include_full+="&"+self.o.query
        #print "[include %s self.o=%s]"%(include_full,self.o)
        self.include(include_full)
        output="[callApp %s] nothing in self.output?]\ninclude: %s\nself.GET: %s"%\
                (appname,include_full,self.GET)
        if len(self.output): # return the last item
            output=copy.copy(self.output[-1])
        self.output=saved # restore the array of outputs
        return output
    def hexdump(self,data):
        if not data:
            return ""
        h=data.encode("hex")
        res=""
        for i in range(0,len(h),64):
            res+=h[i:i+64]+" "
            res+=h[i:i+64].decode("hex")+"\n"
        return res
    def dump(self,var):
        if type(var) is dict:
            for key in var.keys():
                type_=type(var[key])
                value=""
                if type_ is unicode: 
                    value=var[key]
                if type_ is list:
                    value="<items %d>"%len(var[key])
                print type_,key,value
                if type_ in (dict,list):
                    self.dump(var[key]) #recurse
        if type(var) is list:
            for x in var:
                type_=type(x)
                print type_,x

PORT=8082
def run(server_class=HTTPServer, handler_class=S, port=PORT):
    server = ThreadingHTTPServer(('0.0.0.0', port), handler_class)
    print 'Starting httpd on port %d...'%port
    try:
        webbrowser.open('http://localhost:%d'%port, new=1)
    except Exception.e:
        print "[info] %s"%e
    finally:
        server.serve_forever()

if __name__ == "__main__":
    try:
        if len(sys.argv) == 2:
            run(port=int(sys.argv[1]))
        else:
            run()
    except KeyboardInterrupt:
        print '^C received, shutting down the web server'
        os._exit(0)