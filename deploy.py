#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__="@m_ndingo"

import sys,os,re

assert os.path.exists(sys.argv[1])

mainapp_name=re.sub("[/\\\.]+","_",sys.argv[1])

print "#!/usr/bin/python"
print "# -*- coding: utf-8 -*-"
print "__author__='%s'"%__author__
print "import sys, os, re, json, time, traceback, socket, copy"
print "from threading import Timer"
print "class Piwy_App():"
print "\tdef __init__(self):"
print """\t\tself.path="%s"
		self.debug     = False
		self.HOME_HTML = "files"
		self.GET       = {"mode":"raw"}
		self.output    = []
		for i in range(1,len(sys.argv)-1):
			self.GET[sys.argv[i]]=sys.argv[i+1]"""%(os.path.dirname(sys.argv[1]))
print "\t\tself.run_%s()"%mainapp_name
print "\t\tself.flush()"
print """\tdef include(self,appname):
		if self.debug: print "including: %s"%appname
	def out(self,msg,**kargs):
		box   = kargs.get("box")
		li    = kargs.get("li")
		style = kargs.get("style")
		for k,v in kargs.iteritems():
			if k=="cr" : continue
			if k=="box": continue
			if k=="li" : continue
			if k=="style":	
				styles  = v.split(" ")
				missing = []
				for i in range(len(styles)):
					style     = styles[i]
					missed    = True
					if style in ("info","term","title"): 
						missed = False
					if style=="leftpush": 
						missed = False
						box    = True
					if style=="semidark": 
						msg="\033[2m%s\033[0m"%msg
						missed = False
					if style=="note":
						msg="\033[43;1;30m%s\033[0m"%msg
						missed = False
					if missed:
						missing.append(style)
				if len(missing):
					print "{decoration}"," ".join(missing)
			if k=="div" and v==False: continue
		if box:
			tmp=""
			for line in msg.split("\\n"):
				tmp+="%s %s\\n"%(3*"#",line)
			msg=tmp.rstrip()
			box=False
		if li:
			msg="- "+msg
			li=False
		if style:
			msg="\033[1m%s\033[0m"%msg
		if len(str(msg))>1024:
			self.output.append("[unk] %s > 1024 bytes"%(type(msg)))
		else:
			self.output.append(msg)
	def flush(self):
		EMPTY='empty buffer'
		for item in self.output:
			if item!=EMPTY:
				print item
		self.output=[EMPTY]"""
print "\tdef escape(self,msg,**kargs):"
print "\t\treturn msg"
print "\tdef callApp(self,appname,params={}):"
print """\t\tif self.debug: print 'callApp: path=%s appname=%s'%(self.path,appname)
		if appname.startswith("/"):
			funcname="self.run_"+re.sub("[/\\\.]+","_",self.HOME_HTML+"/"+appname)
		else:
			funcname="self.run_"+re.sub("[/\\\.]+","_",self.path+"/"+appname)
		if self.debug:
			print 'calling function: %s'%funcname
			print 'self.GET:',self.GET
			print 'params:',params
		for k,v in params.iteritems():
			self.GET[k]=v
		eval(funcname+"()")
		res=self.output[-1]
		self.output=self.output[:-1] # remove the last buffer
		self.flush()"""
print "\t\treturn res"
print """\tdef read(self,url,mode="html",mark=None,cookies={},timeout=5):
		import requests,json
		try:
			r = requests.get(url, cookies=cookies,timeout=timeout)
			data=r.text
			if mode=="json":
				data=r.text[r.text.index(mark)+len(mark)+1:-2]
				data=json.loads(data)
			return data
		except Exception,e:
			return e"""
for fname in sys.argv[1:]:
	loc=open(fname,"rb").read().split("\n")
	print "\t# sourcode: %s"%fname
	fname=re.sub("[/\\\.]+","_",fname)
	print "\tdef run_%s(self):"%fname
	for line in loc:
		print "\t\t%s"%line
print "\nPiwy_App()"